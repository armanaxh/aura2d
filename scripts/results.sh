RESULT_DIR="/home/armanaxh/Documents/workSpace/results/"

while [ $# -gt 0 ]
do
  case $1 in
    -t1|--first_team)
      filter_name_1="${2}"
      shift 1
      ;;


    -t2|--second_team)
      filter_name_2="${2}"
      shift 1
      ;;

    -rd|--result_dir)
      RESULT_DIR=$RESULT_DIR"${2}"
      shift 1
      ;;

    *)
      echo 1>&2
      echo "invalid option \"${1}\"." 1>&2
      echo 1>&2
      exit 1
      ;;
  esac

  shift 1
done




# if [ -z "$filter_name_1"]
# then
  # echo "true"
# fi

total_team_goal_1=0
total_team_goal_2=0
win_game_team1=0
win_game_team2=0
match_number=0



cd $RESULT_DIR


STYLE_PAGE="table {width:100%;}table, th, td {border: 1px solid black;border-collapse: collapse;}th, td {padding: 15px;text-align: left;}table#t01 tr:nth-child(even) {background-color: #eee;}table#t01 tr:nth-child(odd) {background-color: #fff;}table#t01 th{background-color: black;color: white;} .half_w { float:left;width: 45%; margin-left: 10px; margin-right: 10px;}"
echo "<!DOCTYPE html><html><head><style>$STYLE_PAGE</style></head><body>"
echo "<div class='half_w'>"
echo "<table id='t01'>"
echo "<tr><th>Team 1</th><th><th></th><th>Team2</th></tr>"




for x in *.rcg; do
  re="^([0-9]+)-(.*)_([0-9]+)-vs-(.*)_([0-9]+).rcg$"
  [[ $x =~ $re ]] && res_time="${BASH_REMATCH[1]}" && team_name1="${BASH_REMATCH[2]}" && team_goal_1="${BASH_REMATCH[3]}" && team_name2="${BASH_REMATCH[4]}" && team_goal_2="${BASH_REMATCH[5]}"


  if ! [ -z "$filter_name_1" ]
  then
    if [ "$filter_name_1" == "$team_name1" ]; then
      echo ""
    else
      continue
    fi
  fi
  if ! [ -z "$filter_name_2" ]
  then
    if [ "$filter_name_2" == "$team_name2" ]; then
      echo ""
    else
      continue
    fi
  fi




  total_team_goal_1=$((team_goal_1+total_team_goal_1))
  total_team_goal_2=$((team_goal_2+total_team_goal_2))
  match_number=$((match_number+1));
  if [ $team_goal_1 -gt $team_goal_2 ]
  then
    win_game_team1=$((win_game_team1+1))
  fi

  if [ $team_goal_2 -gt $team_goal_1 ]
  then
    win_game_team2=$((win_game_team2+1))
  fi


  echo "<tr>"
  echo "<td>$team_name1</td>"
  echo "<td>$team_goal_1</td>"
  echo "<td>$team_goal_2</td>"
  echo "<td>$team_name2</td>"
  echo "<tr>"

done


echo "</table></div>"

######################################################################## result

echo "<div class='half_w'>"
echo "<table id='t01'>"
echo "<tr><th>Title </th><th>Value</th></tr>"


echo "<tr>""<td>Total taken Goal (Our) </td><td>$total_team_goal_1</td>""</tr>"
echo "<tr>""<td>Total given Goal (their) </td>""<td>$total_team_goal_2</td>""<tr>"
echo "<tr>""<td>Average taken Goal (Our) </td>""<td>$(echo "$total_team_goal_1/$match_number" | bc -l)</td>""<tr>" | sed 's/..$/.&/'
echo "<tr>""<td>Average given Goal (their) </td>""<td>$(echo "$total_team_goal_2/$match_number" | bc -l)</td>""<tr>"
echo "<tr>""<td>match number  </td>""<td>$match_number</td>""<tr>"

echo "<tr>""<td>number of win </td>""<td>$win_game_team1</td>""<tr>"
echo "<tr>""<td>number of lost </td>""<td>$win_game_team2</td>""<tr>"
echo "<tr>""<td> win %:  </td>""<td> $(echo "$win_game_team1/$match_number*100" | bc -l) </td>""<tr>"
echo "<tr>""<td> win and == %:  </td>""<td> $(echo "($match_number-$win_game_team2)/$match_number*100" | bc -l) </td>""<tr>"




echo "</table></div>"

echo "</body></html>"
