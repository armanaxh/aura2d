TEAM_DIR=$1

declare -a necessary_files=("startAll")


for x in */; do
	cd $x
	for tmp_file in "${necessary_files[@]}"
	do
		if [ ! -f "$tmp_file" ]; then
			echo "$tmp_file in team $x not exist! --- "
		fi
	done
	cd ..
done
