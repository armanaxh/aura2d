


# declare -a our_version=("Aura2D" "Aura2D_c_a" "Aura2D_r_t_l")
declare -a our_version=("Aura2D_dgp")

declare -a teams=("cyros-2016" "HERMES" "CYRUS2019")

RESULT_DIR_NAME="day7"


for our_ver in "${our_version[@]}"
do
  for team in "${teams[@]}"
  do
    bash results.sh -t1 $our_ver -t2 $team -rd $RESULT_DIR_NAME > "./t_res/$RESULT_DIR_NAME"_""$our_ver"_"$team".html"
  done
done
