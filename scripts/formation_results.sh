RESULT_DIR="/home/armanaxh/Documents/workSpace/results"

cd $RESULT_DIR

STYLE_PAGE="table {width:100%;}table, th, td {border: 1px solid black;border-collapse: collapse;}th, td {padding: 15px;text-align: left;}table#t01 tr:nth-child(even) {background-color: #eee;}table#t01 tr:nth-child(odd) {background-color: #fff;}table#t01 th{background-color: black;color: white;}"
echo "<!DOCTYPE html><html><head><style>$STYLE_PAGE</style></head><body>"
echo "<table id='t01'>"
echo "<tr><th>Team 1</th><th><th></th></th><th>Team2</th><th>Strategy</th></tr>"



temp_formation_name="NONE"
for x in *.rcg; do

  if [ ${x: -6} == ".conf2" ]; then
    re="^([0-9]+)([a-zA-Z](.)*).conf2$"
    [[ $x =~ $re ]] && var1="${BASH_REMATCH[1]}" && var2="${BASH_REMATCH[2]}"
    temp_formation_name=$var2
    continue
  fi


  # #20191013104552-HELIOS_org_0-vs-Oxsy_3.rcg

  re="^([0-9]+)-(.*)_([0-9]+)-vs-(.*)_([0-9]+).rcg$"
  [[ $x =~ $re ]] && res_time="${BASH_REMATCH[1]}" && team_name1="${BASH_REMATCH[2]}" && team_goal_1="${BASH_REMATCH[3]}" && team_name2="${BASH_REMATCH[4]}" && team_goal_2="${BASH_REMATCH[5]}"


  echo "<tr>"
  echo "<td>$team_name1</td>"
  echo "<td>$team_goal_1</td>"
  echo "<td>$team_goal_2</td>"
  echo "<td>$team_name2</td>"
  echo "<td>$temp_formation_name</td>"
  echo "<tr>"


done


echo "</table>"
echo "</body></html>"
