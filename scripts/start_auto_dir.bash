BASE_DIR="/home/armanaxh/Documents/workSpace/evalTeam/"

LOG_DIT="~/rcss_game_log/"
SLEEP_AFTER=720

killsmp="/home/armanaxh/Documents/workSpace/cmds/killall.sh"

team_aura2d="aura2d/src/"
aura2d_counter_attack="aura2d_counter_attack/src/"
aura2d_run_to_line="aura2d_run_to_line/src/"
aura2d_without_mark="aura2d_with_mark/src/"
aura2d_hard_mark="aura2d_hard_mark/src/"
aura2d_deg_pass="aura2d_dgp/src/"


declare -a our_version=($team_aura2d)

DIR_PATH=$1
for i in {1..30}
do
  for version_team in "${our_version[@]}"
  do
    team_1=$BASE_DIR$version_team

    for team in $BASE_DIR$DIR_PATH/*/
    do

      team_2=$team
      # $BASE_DIR$team

      #change Formation

      # echo $FORMATION_DIR


      # cd $FORMATION_DIR
      # cp $FORMATION_DIR$x $team_formation_dir$formation_name
      # for x in *.conf; do
      #   # echo "File Formation $x";
      #   rm $team_formation_dir$formation_name
      #   cp $FORMATION_DIR$x $team_formation_dir$formation_name
      #   echo  "$x  ->>>  $formation_name"
      #   timee=$(date  +%Y%m%d%H%M%S)
      #   echo $timee
      #   touch ~/rcss_game_log/$timee$x"2"
        cd $BASE_DIR$DIR_PATH

        sleep 2
        echo $our_team" -----> "$team
        cd ~
        rcsoccersim &
        sleep 1
        cd $team_1
        $team_1"/start.sh" --debug &
        sleep 1
        cd $team_2
        $team_2"startAll" &

        sleep $SLEEP_AFTER
        $killsmp

        echo "===================================--"
        sleep 1

      # done
    done

  done
done
