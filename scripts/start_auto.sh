BASE_DIR="/home/armanaxh/Documents/workSpace/evalTeam/"

LOG_DIT="~/rcss_game_log/"

killsmp="/home/armanaxh/Documents/workSpace/cmds/killall.sh"


our_team="cafe_2d/src"
team_formation_dir="/home/armanaxh/Documents/workSpace/evalTeam/cafe_2d/src/formations-dt/"

# team path
team_cyrous2016="cyrus-2016/src"
team_cyrous2019="cyrus-2019/"
team_oxsy="oxsy_2016"
team_hermes="Hermes"
team_base_g="G2d-2.6/src/"


team_aura2d="aura2d/src/"
aura2d_counter_attack="aura2d_counter_attack/src/"
aura2d_run_to_line="aura2d_run_to_line/src/"
aura2d_without_mark="aura2d_with_mark/src/"
aura2d_hard_mark="aura2d_hard_mark/src/"
aura2d_deg_pass="aura2d_dgp/src/"


declare -a our_version=($aura2d_deg_pass $team_aura2d)
declare -a teams=($team_cyrous2016 $team_cyrous2019 $team_hermes )
# declare -a teams=($team_oxsy)
SLEEP_AFTER=720


formation_type="offense"
formation_name="offense-formation.conf"
FORMATION_DIR=$BASE_DIR"formations/"$formation_type"/"

# $killsmp
# cp $BASE_DIR"formations/defense/defense-formation.conf" $team_formation_dir"defense-formation.conf"
# cp $BASE_DIR"formations/offense/offense-formation.conf" $team_formation_dir"offense-formation.conf"
for i in {1..30}
do
  for version_team in "${our_version[@]}"
  do
    team_1=$BASE_DIR$version_team

    for team in "${teams[@]}"
    do

      team_2=$BASE_DIR$team
      # $BASE_DIR$team

      #change Formation

      # echo $FORMATION_DIR


      # cd $FORMATION_DIR
      # cp $FORMATION_DIR$x $team_formation_dir$formation_name
      # for x in *.conf; do
      #   # echo "File Formation $x";
      #   rm $team_formation_dir$formation_name
      #   cp $FORMATION_DIR$x $team_formation_dir$formation_name
      #   echo  "$x  ->>>  $formation_name"
      #   timee=$(date  +%Y%m%d%H%M%S)
      #   echo $timee
      #   touch ~/rcss_game_log/$timee$x"2"
        cd $BASE_DIR
        sleep 2
        echo $our_team" ----------------------------------------> "$team
        cd ~
        rcsoccersim &
        sleep 1
        cd $team_1
        $team_1"/start.sh" --debug &
        sleep 1
        cd $team_2
        $team_2"/startAll" &

        sleep $SLEEP_AFTER
        $killsmp

        echo "----------------------------------------------------------------------==================================================--"
        sleep 1

      # done
    done

  done
done
