RESULT_DIR="/home/armanaxh/Documents/workSpace/results"

cd $RESULT_DIR
for x in *.conf; do
  echo $x
  re="^([0-9]+)([a-zA-Z](.)*).conf$"
  [[ $x =~ $re ]] && var1="${BASH_REMATCH[1]}" && var2="${BASH_REMATCH[2]}"
  date_formated=$(date -d @$(  echo $var1 )  +%Y%m%d%H%M%S)

  touch $date_formated"_"$var2".conf2"
  rm $x
done
