//
// Created by armanaxh on ۲۰۱۹/۱۱/۳.
//

#ifndef AURA2D_AURA_MODEL_H
#define AURA2D_AURA_MODEL_H

#include "./utils/HERMES_FastIC.h"
#include <rcsc/player/world_model.h>
#include <rcsc/common/server_param.h>
#include <rcsc/player/player_object.h>
#include <rcsc/player/player_agent.h>

class AuraModel {

    typedef const boost::shared_ptr <rcsc::WorldModel> WorldModelPtr;

private:

    const rcsc::WorldModel *wm;
    static FastIC *fic;

    AuraModel() {}

    AuraModel(const AuraModel &);

    const AuraModel &operator=(const AuraModel &);

public:

    static AuraModel &instance();

    static FastIC *fastIC();

    static
    const
    AuraModel &i() {
        return instance();
    }


    void create(const rcsc::WorldModel &wm, rcsc::PlayerAgent *agent);

    void update(rcsc::PlayerAgent *agent);


    rcsc::Vector2D getBallLordPos() const;

    const rcsc::PlayerObject *getBallLord() const;


};


#endif //AURA2D_AURA_MODEL_H
