//
// Created by armanaxh on ۲۰۱۹/۱۱/۳.
//

#include "aura_model.h"

#include <rcsc/player/intercept_table.h>

using namespace rcsc;

FastIC *AuraModel::fic;

AuraModel &AuraModel::instance() {
    static AuraModel s_instance;
    return s_instance;
}

void AuraModel::create(const rcsc::WorldModel &wm, PlayerAgent *agent) {
    this->wm = &wm;
}

FastIC *AuraModel::fastIC() {
    return AuraModel::fic;
}


void AuraModel::update(PlayerAgent *agent) {
    fic = new FastIC(agent);
}





rcsc::Vector2D AuraModel::getBallLordPos() const {
    const PlayerObject *ball_lord = getBallLord();
    if (ball_lord) { return ball_lord->pos(); }
    return Vector2D::INVALIDATED;
}

const PlayerObject *AuraModel::getBallLord() const {
    const PlayerObject *lord = NULL;
    if (wm->ball().pos() == Vector2D::INVALIDATED) {
        return lord;
    }
    const InterceptTable *interceptTable = wm->interceptTable();


    const int mate_min = interceptTable->teammateReachCycle();
    const int opp_min = interceptTable->opponentReachCycle();
    const int self_min = interceptTable->selfReachCycle();

    if (mate_min < opp_min) {
        if (interceptTable->fastestTeammate())
            return interceptTable->fastestTeammate();
    } else {
        if (interceptTable->fastestOpponent())
            return interceptTable->fastestOpponent();
    }

    return lord;
}
