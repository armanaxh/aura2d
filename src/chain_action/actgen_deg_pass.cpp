//
// Created by armanaxh on ۲۰۱۹/۱۱/۵.
//

#include "actgen_deg_pass.h"

#include "pass.h"

#include "action_state_pair.h"
#include "predict_state.h"
#include "pass.h"
#include "../utils/rcsc_utils.h"

#include "field_analyzer.h"
#include <rcsc/player/player_object.h>
#include <rcsc/common/logger.h>

#include "../strategy.h"
#include "../aura_model.h"


void ActGen_DegPass::generate(std::vector <ActionStatePair> *result, const PredictState &state,
                              const rcsc::WorldModel &wm, const std::vector <ActionStatePair> &path) const {

    getDegPass(wm, state, result);
}


void ActGen_DegPass::getDegPass(const rcsc::WorldModel &wm, const PredictState &state,
                                std::vector <ActionStatePair> *result) const {

    const ServerParam &SP = ServerParam::i();
    const Strategy &stra = Strategy::i();
    const AuraModel &aura_model = AuraModel::i();

    double offside_line_x = wm.offsideLineX();

    FastIC *fic = aura_model.fastIC();
    configFastIC(fic, wm);

    const PredictBallObject ball_object = state.ball();
    Vector2D ball_pos = ball_object.pos();
    double ball_speed = ball_object.pos().r();
    AngleDeg angle_ball = ball_object.vel().th();



    double pass_speed_step = 0.25;
    for (double pass_speed = 1.0; pass_speed < 3; pass_speed += pass_speed_step) {


        Vector2D temp_dir(pass_speed, 0);
        double step_deg = 5;

        for (double i = 0; i < 360; i += step_deg) {

            temp_dir = temp_dir.rotate(step_deg);

            fic->refresh();
            fic->setBall(ball_pos, temp_dir, 0);
            fic->calculate();

            const int mate_reach = fic->getFastestTeammateReachCycle();
            const int opp_reach = fic->getFastestOpponentReachCycle();

            if (mate_reach < opp_reach && mate_reach < 20) {


                const AbstractPlayerObject *fastest_mate = fic->getFastestTeammate(false);
                if (fastest_mate == NULL) {
                    continue;
                }


                const int receiver_unum = fastest_mate->unum();
                if (receiver_unum == -1) {
                    continue;
                }


                Vector2D receiver_pos = fastest_mate->pos();
                if (receiver_pos == Vector2D::INVALIDATED) {
                    continue;
                }

                if(receiver_pos.x > offside_line_x){
                    continue;
                }

                if(receiver_pos.dist(ball_pos) < 12 && pass_speed > 2.6){
                    continue;
                }

                if(receiver_pos.dist(ball_pos) < 5){
                    continue;
                }


                const AbstractPlayerObject *ball_holder = state.ballHolder();
                if (ball_holder == NULL) {
                    return;
                }

                const int ball_holder_unum = ball_holder->unum();

                Vector2D resive_pass_pos = fic->getFastestTeammateReachPoint(false);
                if (resive_pass_pos == Vector2D::INVALIDATED) {
                    continue;
                }
                double dist_pass = rcscUtils::ballPathDist(mate_reach, pass_speed);

                Vector2D pass_vel = temp_dir;
                pass_vel.setLength(dist_pass);
                Vector2D pass_pos = resive_pass_pos;//ball_pos + pass_vel;
                if (dist_pass < 5) {
                    continue;
                }

                if (std::abs(pass_pos.x) > 50 || std::abs(pass_pos.y) > 32) {
                    continue;
                }

                if (pass_pos == Vector2D::INVALIDATED) {
                    continue;
                }


                int kick_count = FieldAnalyzer::predict_kick_count(wm,
                                                                   ball_holder,
                                                                   dist_pass,
                                                                   angle_ball);

                dlog.addLine(Logger::PASS,
                             ball_pos, pass_pos,
                             "#3250a8");



                CooperativeAction::Ptr pass_temp(new Pass(ball_holder_unum,
                                                          receiver_unum,
                                                          pass_pos,
                                                          pass_speed, mate_reach, kick_count, 0,
                                                          "description deg pass"));


                PredictState temp_test(state,
                                       mate_reach,
                                       receiver_unum,
                                       pass_pos);

                PredictState *temp_predict_state(new PredictState(state,
                                                                  mate_reach,
                                                                  receiver_unum,
                                                                  pass_pos));

                ActionStatePair temp_action_state_pair(pass_temp,
                                                       temp_predict_state);


                result->push_back(temp_action_state_pair);

            }

        }
    }

}


void ActGen_DegPass::configFastIC(FastIC *fastIC, const rcsc::WorldModel &wm) const {

    const ServerParam &SP = ServerParam::i();

    fastIC->reset();
    fastIC->setShootMode();
    fastIC->setProbMode();

    int player_delay = 4;
    double ball_x = wm.ball().pos().x;
    double ball_y = wm.ball().pos().y;

    if(ball_x > -8){
        player_delay = 3;
    }else if (ball_x > 10){
        player_delay = 2;
    }else if(ball_x > 32){
        player_delay = 2;
    }else if(Strategy::get_ball_area(wm) == Strategy::BA_ShootChance && std::abs(ball_y) < 7){
        player_delay = 1;
    }


    double kickable_area_s = 0.5;
    if (wm.ball().pos().x > 33) {
        kickable_area_s = 1;
    }

    for (int i = 0; i < wm.ourPlayers().size(); i++) {
        if (fastIC->isPlayerValid(wm.ourPlayers()[i])) {
            if (wm.ourPlayers()[i]->unum() != wm.self().unum()) {
                fastIC->addPlayer(wm.ourPlayers()[i], kickable_area_s, 1.0, player_delay);
            }
        }
    }
    for (int i = 0; i < wm.theirPlayers().size(); i++) {
        if (fastIC->isPlayerValid(wm.theirPlayers()[i])) {
            fastIC->addPlayer(wm.theirPlayers()[i]);
        }
    }

    fastIC->setMaxCycleAfterFirstFastestPlayer(10);
    fastIC->setMaxCycleAfterOppReach(10);
    fastIC->setMaxCycles(20);


}