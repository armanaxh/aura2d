//
// Created by armanaxh on ۲۰۱۹/۱۱/۵.
//

#ifndef AURA2D_ACTGEN_DEG_PASS_H
#define AURA2D_ACTGEN_DEG_PASS_H

#include "action_generator.h"
#include "../utils/HERMES_FastIC.h"

class ActGen_DegPass
        : public ActionGenerator {

public:
    virtual
    void generate( std::vector< ActionStatePair > * result,
                   const PredictState & state,
                   const rcsc::WorldModel & wm,
                   const std::vector< ActionStatePair > & path ) const;

private:
    void getDegPass(const rcsc::WorldModel & current_wm, const PredictState &state, std::vector< ActionStatePair > * result) const;
    void configFastIC(FastIC * fic, const rcsc::WorldModel &wm) const;
};
#endif //AURA2D_ACTGEN_DEG_PASS_H
