//
// Created by armanaxh on ۲۰۱۹/۱۱/۲.
//

#include "bhv_offensive_positioning.h"

#include "../../strategy.h"
#include "../../aura_model.h"
#include <rcsc/action/neck_turn_to_ball_or_scan.h>
#include <rcsc/action/basic_actions.h>
#include <rcsc/action/body_go_to_point.h>
#include <rcsc/player/intercept_table.h>

#include <rcsc/player/player_agent.h>
#include <rcsc/player/debug_client.h>
#include <rcsc/common/logger.h>
#include <rcsc/common/server_param.h>

using namespace rcsc;

bool Bhv_OffensivePositioning::execute(rcsc::PlayerAgent *agent) {
    dlog.addText(Logger::TEAM,
                 __FILE__": Bhv_OffensivePositioning ");

    const WorldModel &wm = agent->world();

    Vector2D best_pos = getBestPos(agent);

    if (best_pos != Vector2D::INVALIDATED) {

        Vector2D target_pos = best_pos;
        const double dash_power = Strategy::get_normal_dash_power(wm);

        double dist_thr = wm.ball().distFromSelf() * 0.15;
        if (dist_thr < 1.0) dist_thr = 1.0;

        dlog.addText(Logger::TEAM,
                     __FILE__": Bhv_OffensivePositioning target=(%.1f %.1f) dist_thr=%.2f",
                     target_pos.x, target_pos.y,
                     dist_thr);

        agent->debugClient().addMessage("Bhv_OffensivePositioning%.0f", dash_power);
        agent->debugClient().setTarget(target_pos);
        agent->debugClient().addCircle(target_pos, dist_thr);

        if (!Body_GoToPoint(target_pos, dist_thr, dash_power
        ).execute(agent)) {
            Body_TurnToBall().execute(agent);
        }

        if (wm.existKickableOpponent()
            && wm.ball().distFromSelf() < 18.0) {
            agent->setNeckAction(new Neck_TurnToBall());
        } else {
            agent->setNeckAction(new Neck_TurnToBallOrScan());
        }
        return true;
    }

    return false;
}


///////////////////////DEBUG

static std::vector <std::vector<double> > table_final_score;

///////////////////////////////

rcsc::Vector2D Bhv_OffensivePositioning::getBestPos(rcsc::PlayerAgent *agent) {

    const WorldModel &wm = agent->world();
//
//    Vector2D self_form_pos = Strategy::i().getPosition(wm.self().unum()); // TODO check self_pos is ok
    Vector2D self_pos = wm.self().pos();
//    Vector2D ball_lord_pos = AuraModel::instance().getBallLordPos();
    Vector2D ball_pos = wm.ball().pos();

    int x_offside = wm.offsideLineX();

    double search_radius = 5;//TODO dynamic
    std::pair<double, double> check_line_y(self_form_pos.y - search_radius, self_form_pos.y + search_radius);
    std::pair<double, double> check_line_x(self_form_pos.x - search_radius, self_form_pos.x + search_radius);

    double max_score = 0;
    Vector2D best_pos = Vector2D::INVALIDATED;

    ////DEBUG

    table_final_score.clear();
    ///////////////////////////////////////

    for (int i = check_line_x.first; i <= check_line_x.second; i += 2) {
        //////
        table_final_score.push_back(std::vector<double>());
        ////////////////////////////
        for (int j = check_line_y.first; j <= check_line_y.second; j += 2) {
            Vector2D check_point(i, j);


            if (!checkPosIsValid(check_point, self_pos, ball_pos, x_offside, wm)) {
                ///////////
                table_final_score.back().push_back(0);
                ///////////////////////////////////////////////////
                continue;
            }

            double temp_score = 0;

            double near_to_goal = nearToGoal(check_point, 2 * search_radius) * 1;
            double near_to_ball = nearToBall(check_point, ball_pos, 2 * search_radius) * 1;

            temp_score += near_to_goal;
            temp_score += near_to_ball;

            if (max_score < temp_score) {
                max_score = temp_score;
                best_pos = check_point;
            }

            dlog.addCircle(Logger::TEAM,
                           check_point, 0.5, "#0ff000", false);

            table_final_score.back().push_back(temp_score);
        }
    }

    log_table(table_final_score, "final score ");

    dlog.addCircle(Logger::TEAM,
                   best_pos, 0.5, "#000000", true);


    return best_pos;
}


bool Bhv_OffensivePositioning::checkPosIsValid(rcsc::Vector2D check_point, rcsc::Vector2D self_pos,
                                               rcsc::Vector2D ball_pos, const double x_offside, const rcsc::WorldModel &wm) {


    if (check_point.x > x_offside - 1) {
        return false;
    }

    if (std::abs(check_point.y) > 34) {
        return false;
    }


    const PlayerPtrCont::const_iterator t_end = wm.teammatesFromSelf().end();
    for (PlayerPtrCont::const_iterator t = wm.teammatesFromSelf().begin();
         t != t_end;
         ++t) {
        if (!(*t) || (*t)->unum() == wm.self().unum()) {
            continue;
        }
        if ((*t)->pos().dist(check_point) < 3.5) {
            return false;
        }
    }

    const PlayerPtrCont::const_iterator end = wm.opponentsFromSelf().end();
    for (PlayerPtrCont::const_iterator o = wm.opponentsFromSelf().begin();
         o != end;
         ++o) {
        if ((*o) && (*o)->pos().dist(check_point) < 4) {
            return false;
        }
    }


    return true;
}


double Bhv_OffensivePositioning::nearToGoal(rcsc::Vector2D check_point, double max_radius2) {
    const ServerParam &SP = ServerParam::i();
    const Vector2D our_goal = SP.theirTeamGoalPos();
    double dist_from_goal = our_goal.dist(check_point);
    return (55 - dist_from_goal) / 55 ;
}

double Bhv_OffensivePositioning::nearToBall(rcsc::Vector2D check_point, rcsc::Vector2D ball_pos, double max_radius2) {
    double dist_from_ball = ball_pos.dist(check_point);
    return (55 - dist_from_ball) / 55 ;
}


#include "../../utils/utils.cpp"

void Bhv_OffensivePositioning::log_table(std::vector <std::vector<double> > table, std::string name) {
    dlog.addText(Logger::TEAM,
                 __FILE__":   %s ----------------- %d", name.c_str(), table.size());


    int max_d = table.size();
    int arr[max_d][max_d] = {0};

    for (int i = 0; i < table.size(); i++) {
        std::string temp;
        for (int j = 0; j < table[i].size(); j++) {
            arr[i][j] = int(table[i][j] * 100);
        }
    }


    int arr2[max_d][max_d];
    for (int i = 0; i < max_d; ++i) {
        for (int j = 0; j < max_d; ++j) {
            arr2[i][j] = arr[j][i];
        }
    }


    std::string temp;
    for (int i = 0; i < max_d; i++) {
        temp += "   \t";
        for (int j = 0; j < max_d; j++) {
            temp += patch::to_string(arr2[i][j]) + "\t";
        }
        rcsc::dlog.addText(rcsc::Logger::TEAM,
                           __FILE__" %s ", temp.c_str());
        temp = "";
    }

}