//
// Created by armanaxh on ۲۰۱۹/۱۱/۳.
//

#ifndef AURA2D_BHV_WING_TACTIC_H
#define AURA2D_BHV_WING_TACTIC_H


#include <rcsc/player/soccer_action.h>

class Bhv_WingTactic
        : public rcsc::SoccerBehavior {
public:
    Bhv_WingTactic()
    { }

    bool execute( rcsc::PlayerAgent * agent );

private:

};


#endif //AURA2D_BHV_WING_TACTIC_H
