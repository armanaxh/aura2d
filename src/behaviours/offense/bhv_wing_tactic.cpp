//
// Created by armanaxh on ۲۰۱۹/۱۱/۳.
//

#include "bhv_wing_tactic.h"

#include "../../strategy.h"
#include <rcsc/action/neck_turn_to_ball_or_scan.h>
#include <rcsc/action/basic_actions.h>
#include <rcsc/action/body_go_to_point.h>
#include <rcsc/player/intercept_table.h>

#include <rcsc/player/player_agent.h>
#include <rcsc/player/debug_client.h>
#include <rcsc/common/logger.h>
#include <rcsc/common/server_param.h>

using namespace rcsc;

bool Bhv_WingTactic::execute(rcsc::PlayerAgent *agent) {
    dlog.addText(Logger::TEAM,
                 __FILE__": Bhv_WingTactic ");

    const WorldModel &wm = agent->world();

    const int self_unum = wm.self().unum();

    int self_min = wm.interceptTable()->selfReachCycle();
    int mate_min = wm.interceptTable()->teammateReachCycle();
    int opp_min = wm.interceptTable()->opponentReachCycle();

    const int our_min = std::min(self_min, mate_min);



    bool cyrus_2016 = false;
    bool Hermes = false;
    bool cyrus_2019 = false;

    if (wm.opponentTeamName().find("cyros-2016") != std::string::npos)
        cyrus_2016 = true;
    else if (wm.opponentTeamName().find("HERMES") != std::string::npos)
        Hermes = true;
    else if (wm.opponentTeamName().find("CYRUS2019") != std::string::npos)
        cyrus_2019 = true;

// G2d: various states
    bool indFK = false;
    if ((wm.gameMode().type() == GameMode::BackPass_
         && wm.gameMode().side() == wm.theirSide())
        || (wm.gameMode().type() == GameMode::IndFreeKick_
            && wm.gameMode().side() == wm.ourSide())
        || (wm.gameMode().type() == GameMode::FoulCharge_
            && wm.gameMode().side() == wm.theirSide())
        || (wm.gameMode().type() == GameMode::FoulPush_
            && wm.gameMode().side() == wm.theirSide())
            )
        indFK = true;

    bool dirFK = false;
    if (
            (wm.gameMode().type() == GameMode::FreeKick_
             && wm.gameMode().side() == wm.ourSide())
            || (wm.gameMode().type() == GameMode::FoulCharge_
                && wm.gameMode().side() == wm.theirSide())
            || (wm.gameMode().type() == GameMode::FoulPush_
                && wm.gameMode().side() == wm.theirSide())
            )
        dirFK = true;

    bool cornerK = false;
    if (
            (wm.gameMode().type() == GameMode::CornerKick_
             && wm.gameMode().side() == wm.ourSide())
            )
        cornerK = true;

    bool kickin = false;
    if (
            (wm.gameMode().type() == GameMode::KickIn_
             && wm.gameMode().side() == wm.ourSide())
            )
        kickin = true;

// G2d : wing tactic
    double wing_x = -15.0;
    double wing_y = 7.0;
    double wing_depth = 5.0;
    double wing_limit = 39.0;

    if (Hermes) {
        wing_depth = 7.0;
        wing_y = 4.0;
    }

    bool flag_execute_wing = false;
    Vector2D target_pos = Strategy::i().getPosition(self_unum);


    if (our_min < opp_min)
        if (wm.ball().pos().x > wing_x)
            if (wm.ball().pos().x < wing_limit)
                if (fabs(wm.ball().pos().y) > wing_y)
                    if (!indFK && !dirFK && !cornerK && !kickin) {
                        if (self_unum == 9)
                            target_pos.x = wm.offsideLineX() + wm.ball().vel().x;
                        if (self_unum == 10)
                            target_pos.x = wm.offsideLineX() + wm.ball().vel().x;
                        if (self_unum == 11)
                            target_pos.x = wm.offsideLineX() + wm.ball().vel().x;

                        if (wm.ball().pos().y > 0) {
                            if (self_unum == 9)
                                target_pos.y = 15.0;
                            if (self_unum == 11)
                                target_pos.y = 22.5;
                            if (self_unum == 10)
                                target_pos.y = 30.0;
                        } else {
                            if (self_unum == 9)
                                target_pos.y = -30.0;
                            if (self_unum == 11)
                                target_pos.y = -22.5;
                            if (self_unum == 10)
                                target_pos.y = -15.0;
                        }

                        double midX = wm.offsideLineX() - wing_depth;

                        if (self_unum == 6)
                            target_pos.x = midX;
                        if (self_unum == 7)
                            target_pos.x = midX;
                        if (self_unum == 8)
                            target_pos.x = midX;

                        if (wm.ball().pos().y > 0) {
                            if (self_unum == 7)
                                target_pos.y = 15.0;
                            if (self_unum == 6)
                                target_pos.y = 22.5;
                            if (self_unum == 8)
                                target_pos.y = 30.0;
                        } else {
                            if (self_unum == 7)
                                target_pos.y = -30.0;
                            if (self_unum == 6)
                                target_pos.y = -22.5;
                            if (self_unum == 8)
                                target_pos.y = -15.0;
                        }

                        flag_execute_wing = true;
                        dlog.addCircle(Logger::TEAM,
                                       target_pos, 0.5,
                                       "#ff0000");
                    }



    if(flag_execute_wing){

        const double dash_power = Strategy::get_normal_dash_power(wm);

        double dist_thr = wm.ball().distFromSelf() * 0.25;
        if (dist_thr < 1.0) dist_thr = 1.0;

        dlog.addText(Logger::TEAM,
                     __FILE__": Bhv_WingTactic target=(%.1f %.1f) dist_thr=%.2f",
                     target_pos.x, target_pos.y,
                     dist_thr);

        agent->debugClient().addMessage("Bhv_WingTactic%.0f", dash_power);
        agent->debugClient().setTarget(target_pos);
        agent->debugClient().addCircle(target_pos, dist_thr);

        if (!Body_GoToPoint(target_pos, dist_thr, dash_power
        ).execute(agent)) {
            Body_TurnToBall().execute(agent);
        }

        if (wm.existKickableOpponent()
            && wm.ball().distFromSelf() < 18.0) {
            agent->setNeckAction(new Neck_TurnToBall());
        } else {
            agent->setNeckAction(new Neck_TurnToBallOrScan());
        }
        return true;
    }

    return false;
}