//
// Created by armanaxh on ۲۰۱۹/۱۱/۲.
//

#ifndef AURA2D_BHV_OFFENSIVE_POSITIONING_H
#define AURA2D_BHV_OFFENSIVE_POSITIONING_H

#include <rcsc/player/soccer_action.h>
#include <rcsc/geom/vector_2d.h>
#include <rcsc/player/world_model.h>
#include <vector>

#include <rcsc/geom/vector_2d.h>

class Bhv_OffensivePositioning
        : public rcsc::SoccerBehavior {

    rcsc::Vector2D self_form_pos;
public:
    Bhv_OffensivePositioning(rcsc::Vector2D self_form_pos) : self_form_pos(self_form_pos) {}

    bool execute(rcsc::PlayerAgent *agent);

private:
    rcsc::Vector2D getBestPos(rcsc::PlayerAgent *agent);

    bool checkPosIsValid(rcsc::Vector2D check_point, rcsc::Vector2D self_pos, rcsc::Vector2D ball_pos,
                         const double x_offside, const rcsc::WorldModel &wm);

    double nearToGoal(rcsc::Vector2D check_point, double max_radius2);

    double nearToBall(rcsc::Vector2D check_point, rcsc::Vector2D ball_pos, double max_radius2);


    void log_table(std::vector <std::vector<double> > table, std::string name);
};

#endif //AURA2D_BHV_OFFENSIVE_POSITIONING_H
