//
// Created by armanaxh on ۲۰۱۹/۱۱/۲.
//

#ifndef AURA2D_BHV_OFFENSE_H
#define AURA2D_BHV_OFFENSE_H


#include <rcsc/player/soccer_action.h>

class Bhv_Offense
        : public rcsc::SoccerBehavior {
public:
    Bhv_Offense()
    { }

    bool execute( rcsc::PlayerAgent * agent );

private:

};


#endif //AURA2D_BHV_OFFENSE_H
