//
// Created by armanaxh on ۲۰۱۹/۱۱/۲.
//

#include "bhv_offense.h"

#include "../../strategy.h"
#include "./bhv_offensive_positioning.h"
#include "./bhv_wing_tactic.h"
#include "../bhv_intercept.h"

#include <rcsc/player/player_agent.h>
#include <rcsc/player/debug_client.h>
#include <rcsc/common/logger.h>
#include <rcsc/common/server_param.h>

#include <rcsc/geom/vector_2d.h>

using namespace rcsc;

bool Bhv_Offense::execute(rcsc::PlayerAgent *agent) {
    dlog.addText(Logger::TEAM,
                 __FILE__": Bhv_Offense General");

    const WorldModel &wm = agent->world();
    Vector2D self_form_pos = Strategy::i().getPosition(wm.self().unum());

    if (Bhv_Intercept().execute(agent)) {
        return true;
    }
    if (Bhv_WingTactic().execute(agent)) {
        return true;
    }
    if (Bhv_OffensivePositioning(self_form_pos).execute(agent)) {
        return true;
    }

    return false;
}