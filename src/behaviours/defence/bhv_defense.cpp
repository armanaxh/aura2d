//
// Created by armanaxh on ۲۰۱۹/۱۰/۳۰.
//

#include "bhv_defense.h"

#include "../../strategy.h"
#include "./bhv_tackle.h"
#include "./bhv_basic_block.h"
#include "./bhv_pressing.h"
#include "./bhv_offside_trap.h"
#include "./bhv_mark.h"
#include "../bhv_intercept.h"
#include "../bhv_basic_move.h"
#include <rcsc/player/player_agent.h>
#include <rcsc/player/debug_client.h>
#include <rcsc/common/logger.h>
#include <rcsc/common/server_param.h>

using namespace rcsc;

bool Bhv_Defense::execute(rcsc::PlayerAgent *agent) {
    dlog.addText(Logger::TEAM,
                 __FILE__": Bhv_Defense General");

    const WorldModel &wm = agent->world();

    if(Bhv_Tackle().execute(agent)){
        return true;
    }
    if(Bhv_BasicBlock().execute(agent)){
        return true;
    }
    if (Bhv_Intercept().execute(agent)) {
        return true;
    }
    if(Bhv_Pressing().execute(agent)){
        return true;
    }
    if(Bhv_OffsideTrap().execute(agent)){
        return true;
    }
//    if(Bhv_Mark().execute(agent)){
//        return true;
//    }
    return false;
}