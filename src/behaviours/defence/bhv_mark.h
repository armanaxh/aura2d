//
// Created by armanaxh on ۲۰۱۹/۱۱/۲.
//

#ifndef AURA2D_BHV_MARK_H
#define AURA2D_BHV_MARK_H

#include <rcsc/player/soccer_action.h>
#include <rcsc/geom/vector_2d.h>
#include <rcsc/geom/angle_deg.h>

class Bhv_Mark
        : public rcsc::SoccerBehavior {

public:
    Bhv_Mark() {}

    bool execute(rcsc::PlayerAgent *agent);

private:

    bool crossMarkSideBack(rcsc::PlayerAgent *agent);

    bool crossMarkCenterBack(rcsc::PlayerAgent *agent);

    bool crossMarkDefensiveHalf(rcsc::PlayerAgent *agent);

    bool crossMarkOffensiveHalf(rcsc::PlayerAgent *agent);

    bool softMark(rcsc::PlayerAgent *agent);

    void softMarkSideBack2(rcsc::PlayerAgent *agent, rcsc::Vector2D & homePos);

    void softMarkCenterBack(rcsc::PlayerAgent *agent, rcsc::Vector2D & homePos);

    bool espBodyTurns( rcsc::PlayerAgent *agent, rcsc::AngleDeg & bodyAngle );
};


#endif //AURA2D_BHV_MARK_H
