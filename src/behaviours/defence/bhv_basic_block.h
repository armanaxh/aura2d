//
// Created by armanaxh on ۲۰۱۹/۱۰/۳۰.
//

#ifndef AURA2D_BHV_BASIC_BLOCK_H
#define AURA2D_BHV_BASIC_BLOCK_H

#include <rcsc/player/soccer_action.h>
#include <rcsc/geom/vector_2d.h>

class Bhv_BasicBlock
        : public rcsc::SoccerBehavior {
public:
    Bhv_BasicBlock()
    { }

    bool execute( rcsc::PlayerAgent * agent );

private:
};


class Bhv_MarlikBlock
        : public rcsc::SoccerBehavior {
private:

public:

    static bool isInBlockPoint;
    static int timeAtBlockPoint;
    static rcsc::Vector2D opp_static_pos;

    bool execute( rcsc::PlayerAgent * agent );

private:
    bool doInterceptBall2011( rcsc::PlayerAgent * agent );
    bool doBlockMove( rcsc::PlayerAgent * agent );
    rcsc::Vector2D getBlockPoint( rcsc::PlayerAgent * agent );
};

#endif //AURA2D_BHV_BASIC_BLOCK_H
