//
// Created by armanaxh on ۲۰۱۹/۱۰/۳۰.
//

#include "bhv_tackle.h"

#include "../../strategy.h"
#include "../bhv_basic_tackle.h"

#include <rcsc/action/basic_actions.h>
#include <rcsc/action/body_go_to_point.h>
#include <rcsc/player/player_agent.h>
#include <rcsc/player/debug_client.h>
#include <rcsc/player/intercept_table.h>
#include <rcsc/common/logger.h>
#include <rcsc/common/server_param.h>

using namespace rcsc;

bool Bhv_Tackle::execute(rcsc::PlayerAgent *agent) {
    //-----------------------------------------------
    // tackle
    dlog.addText( Logger::TEAM,
                  __FILE__": Bhv_BasicTackle" );


    const WorldModel & wm = agent->world();
    // G2d: tackle probability
    double doTackleProb = 0.8;
    if (wm.ball().pos().x < 0.0)
    {
        doTackleProb = 0.5;
    }

    if ( Bhv_BasicTackle( doTackleProb, 80.0 ).execute( agent ) && !wm.existKickableTeammate() )
    {
        return true;
    }

    return false;
}
