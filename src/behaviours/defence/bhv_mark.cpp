//
// Created by armanaxh on ۲۰۱۹/۱۱/۲.
//

#include "bhv_mark.h"

#include "../../strategy.h"
#include "../../actions/neck_for_mark.h"

#include <rcsc/action/neck_turn_to_ball_or_scan.h>
#include <rcsc/action/basic_actions.h>
#include <rcsc/action/body_go_to_point.h>
#include <rcsc/player/intercept_table.h>
#include <rcsc/player/world_model.h>
#include <rcsc/player/player_agent.h>
#include <rcsc/player/debug_client.h>
#include <rcsc/common/logger.h>
#include <rcsc/common/server_param.h>

using namespace rcsc;

bool Bhv_Mark::execute(rcsc::PlayerAgent *agent) {

    dlog.addText(Logger::TEAM,
                 __FILE__": Bhv_Mark");

    if (crossMarkSideBack(agent)) {
        agent->setNeckAction(new Neck_ForMark());
        return true;
    }
    if (crossMarkCenterBack(agent)) {
        agent->setNeckAction(new Neck_ForMark());
        return true;
    }
    if (crossMarkDefensiveHalf(agent)) {
        agent->setNeckAction(new Neck_ForMark());
        return true;
    }
    if (crossMarkOffensiveHalf(agent)) {
        agent->setNeckAction(new Neck_ForMark());
        return true;
    }


//    if (softMark(agent)) {
//        return true;
//    }
    return false;
}


bool Bhv_Mark::crossMarkCenterBack(PlayerAgent *agent) {
    const WorldModel &wm = agent->world();

    Vector2D homePos = Strategy::i().getPosition(wm.self().unum());
    Vector2D ball = wm.ball().pos();
    Vector2D me = wm.self().pos();
    Vector2D target = Vector2D(0.0, 0.0);

    int num = wm.self().unum();
    double MarkDist = 1.2;

    if (ball.absY() < 15.0)
        MarkDist = 1.0;

    if (ball.absY() < 9.0)
        return false;

//   int myMin = wm.interceptTable()->selfReachCycle();
    int tmmMin = wm.interceptTable()->teammateReachCycle();
    int oppMin = wm.interceptTable()->opponentReachCycle();

    static bool isMarking = false;
    static int oppNo = -2;

    Vector2D S_opp;

    isMarking = false;

    if (ball.x > -20.0 || (ball.x > -36.0 && tmmMin < oppMin)) {
        isMarking = false;
        oppNo = -2;
    }

    if (oppNo != -2) {
        const PlayerPtrCont::const_iterator end = wm.opponentsFromSelf().end();
        for (PlayerPtrCont::const_iterator o = wm.opponentsFromSelf().begin();
             o != end;
             ++o) {
            if ((*o)->unum() == oppNo)
                if ((*o)->pos().x < homePos.x + 4.0)
                    S_opp = (*o)->pos();
                else {
                    oppNo = -2;
                    isMarking = false;
                    break;
                }
        }
    }


    const PlayerPtrCont &opps = wm.opponentsFromSelf();
    const PlayerObject *nearestOpp = (opps.empty() ? static_cast< PlayerObject * >( 0 ) : opps.front());
    const Vector2D opp = (nearestOpp ? nearestOpp->pos() : Vector2D(-1000.0, 0.0));

    double oppDist = opp.dist(homePos);

    if ((((num == 2 && ball.x < -37.0 && ball.y < homePos.y - 3.0) ||
          (num == 3 && ball.x < -37.0 && ball.y > homePos.y + 3.0)) &&
         oppDist < 2.5) || isMarking) {
//      if( oppNo == -2 )
        target = opp + Vector2D::polar2vector(MarkDist, (ball - opp).th());
//      else if( S_opp.x != 0 )
//          target = S_opp + Vector2D::polar2vector( MarkDist , (ball-opp).th() );
//      else
//          return false;

        double dashPower = ServerParam::i().maxDashPower();
        double distThr = 0.4;

        if (!Body_GoToPoint(target, distThr, dashPower,
                            -1.0,    // prefered dash speed. negative value means no preferred speed
                            3,    // preferred reach cycle
                            true,  // save_recovery
                            15.0  // turn angle threshold
        ).execute(agent)) {
            AngleDeg bodyAngle = agent->world().ball().angleFromSelf();
            if (bodyAngle.degree() < 0.0)
                bodyAngle -= 90.0;
            else
                bodyAngle += 90.0;

            Vector2D turnPoint = me;
            turnPoint += Vector2D::polar2vector(10.0, bodyAngle);

            Body_TurnToPoint(turnPoint).execute(agent);
        }

        isMarking = true;
//        amIMarking = true;

        if (oppNo == -2)
            oppNo == nearestOpp->unum();

        agent->setNeckAction(new Neck_TurnToBallOrScan());
        return true;
    }

    return false;
}


bool Bhv_Mark::crossMarkSideBack(PlayerAgent *agent) {
    const WorldModel &wm = agent->world();

    Vector2D homePos = Strategy::i().getPosition(wm.self().unum());
    Vector2D ball = wm.ball().pos();
    Vector2D me = wm.self().pos();
    Vector2D target = Vector2D(0.0, 0.0);

    int num = wm.self().unum();
    double MarkDist = 2.0;

    if (ball.dist(me) < 10.0)
        MarkDist = 1.0;

    if (ball.absY() < 15.0)
        MarkDist = 1.0;

    if (ball.absY() < 9.0)
        return false;

//   int myMin = wm.interceptTable()->selfReachCycle();
    int tmmMin = wm.interceptTable()->teammateReachCycle();
    int oppMin = wm.interceptTable()->opponentReachCycle();

    static bool isMarking = false;
    static int oppNo = -2;

    Vector2D S_opp = Vector2D(0.0, 0.0);

    isMarking = false;

    if (ball.x > -20.0 || (ball.x > -36.0 && tmmMin < oppMin)) {
        isMarking = false;
        oppNo = -2;
    }


    if (oppNo != -2) {
        const PlayerPtrCont::const_iterator end = wm.opponentsFromSelf().end();
        for (PlayerPtrCont::const_iterator o = wm.opponentsFromSelf().begin();
             o != end;
             ++o) {
            if ((*o)->unum() == oppNo)
                if ((*o)->pos().x < homePos.x + 5.0)
                    S_opp = (*o)->pos();
                else {
                    oppNo = -2;
                    isMarking = false;
                    break;
                }
        }
    }


    const PlayerPtrCont &opps = wm.opponentsFromSelf();
    const PlayerObject *nearestOpp = (opps.empty() ? static_cast< PlayerObject * >( 0 ) : opps.front());
    const Vector2D opp = (nearestOpp ? nearestOpp->pos() : Vector2D(-1000.0, 0.0));

    double oppDist = opp.dist(homePos);


    if ((((num == 4 && ball.x < -37.0 && ball.y > 4.0 && opp.y < me.y + 2.0) ||
          (num == 5 && ball.x < -37.0 && ball.y < -4.0 && opp.y > me.y - 2.0)) &&
         oppDist < 10.0 && opp.x < -36.0) || isMarking) {
//      if( oppNo == -2 )
        target = opp + Vector2D::polar2vector(MarkDist, (ball - opp).th());
//      else if( S_opp.x != 0 )
//          target = S_opp + Vector2D::polar2vector( MarkDist , (ball-opp).th() );
//      else
//          return false;

        double dashPower = ServerParam::i().maxDashPower();
        double distThr = 0.4;

        if (!Body_GoToPoint(target, distThr, dashPower,
                            -1.0,    // prefered dash speed. negative value means no preferred speed
                            3,    // preferred reach cycle
                            true,  // save_recovery
                            15.0  // turn angle threshold
        ).execute(agent)) {
            AngleDeg bodyAngle = agent->world().ball().angleFromSelf();
            if (bodyAngle.degree() < 0.0)
                bodyAngle -= 90.0;
            else
                bodyAngle += 90.0;

            Vector2D turnPoint = me;
            turnPoint += Vector2D::polar2vector(10.0, bodyAngle);

            Body_TurnToPoint(turnPoint).execute(agent);
        }

        isMarking = true;
//        amIMarking = true;

        if (oppNo == -2)
            oppNo == nearestOpp->unum();

//      std::cout<<"\nCycle: "<<wm.time().cycle()<<" No "<<wm.self().unum()<<" MArrrrrrrrrrrrrrrrrrkiiiiiiiing "<<"\n";
        agent->setNeckAction(new Neck_TurnToBallOrScan());
        return true;
    }

    return false;
}

bool Bhv_Mark::crossMarkDefensiveHalf(PlayerAgent *agent) {
    const WorldModel &wm = agent->world();

    Vector2D homePos = Strategy::i().getPosition(wm.self().unum());
    Vector2D ball = wm.ball().pos();
    Vector2D me = wm.self().pos();
    Vector2D target = Vector2D(0.0, 0.0);

    int num = wm.self().unum();
    double MarkDist = 1.5;

    if (ball.absY() < 15.0)
        MarkDist = 1.2;

    if (ball.absY() < 9.0)
        return false;

//   int myMin = wm.interceptTable()->selfReachCycle();
    int tmmMin = wm.interceptTable()->teammateReachCycle();
    int oppMin = wm.interceptTable()->opponentReachCycle();

    static bool isMarking = false;
    static int oppNo = -2;

    isMarking = false;

    Vector2D S_opp = Vector2D(0.0, 0.0);

    if (ball.x > -20.0 || (ball.x > -36.0 && tmmMin < oppMin)) {
        isMarking = false;
        oppNo = -2;
    }

    if (oppNo != -2) {
        const PlayerPtrCont::const_iterator end = wm.opponentsFromSelf().end();
        for (PlayerPtrCont::const_iterator o = wm.opponentsFromSelf().begin();
             o != end;
             ++o) {
            if ((*o)->unum() == oppNo)
                if ((*o)->pos().x < homePos.x + 5.0)
                    S_opp = (*o)->pos();
                else {
                    oppNo = -2;
                    isMarking = false;
                    break;
                }
        }
    }

    const PlayerPtrCont &opps = wm.opponentsFromSelf();
    const PlayerObject *nearestOpp = (opps.empty() ? static_cast< PlayerObject * >( 0 ) : opps.front());
    const Vector2D opp = (nearestOpp ? nearestOpp->pos() : Vector2D(-1000.0, 0.0));

    double oppDist = opp.dist(homePos);

    if ((num == 6 && ball.x < -37.0 && ball.absY() > 9.0 &&
         oppDist < 7.0 && opp.x > homePos.x - 4.0) || isMarking) {
//      if( oppNo == -2 )
        target = opp + Vector2D::polar2vector(MarkDist, (ball - opp).th());
//      else if( S_opp.x != 0 )
//          target = S_opp + Vector2D::polar2vector( MarkDist , (ball-opp).th() );
//      else
//          return false;

        double dashPower = ServerParam::i().maxDashPower();
        double distThr = 0.4;

        if (!Body_GoToPoint(target, distThr, dashPower,
                            -1.0,    // prefered dash speed. negative value means no preferred speed
                            100,    // preferred reach cycle
                            true,  // save_recovery
                            20.0  // turn angle threshold
        ).execute(agent)) {
            AngleDeg bodyAngle = agent->world().ball().angleFromSelf();
            if (bodyAngle.degree() < 0.0)
                bodyAngle -= 90.0;
            else
                bodyAngle += 90.0;

            Vector2D turnPoint = me;
            turnPoint += Vector2D::polar2vector(10.0, bodyAngle);

            Body_TurnToPoint(turnPoint).execute(agent);
        }

        isMarking = true;
//        amIMarking = true;

        if (oppNo == -2)
            oppNo == nearestOpp->unum();

        agent->setNeckAction(new Neck_TurnToBallOrScan());
        return true;
    }

    return false;
}

bool Bhv_Mark::crossMarkOffensiveHalf(PlayerAgent *agent) {
    const WorldModel &wm = agent->world();

    Vector2D homePos = Strategy::i().getPosition(wm.self().unum());
    Vector2D ball = wm.ball().pos();
    Vector2D me = wm.self().pos();
    Vector2D target = Vector2D(0.0, 0.0);

    int num = wm.self().unum();
    double MarkDist = 2.0;

    if (ball.absY() < 15.0)
        MarkDist = 1.2;

    if (ball.absY() < 9.0)
        return false;

//   int myMin = wm.interceptTable()->selfReachCycle();
    int tmmMin = wm.interceptTable()->teammateReachCycle();
    int oppMin = wm.interceptTable()->opponentReachCycle();

    static bool isMarking = false;
    static int oppNo = -2;

    isMarking = false;

    Vector2D S_opp = Vector2D(0.0, 0.0);

    if (ball.x > -20.0 || (ball.x > -36.0 && tmmMin < oppMin)) {
        isMarking = false;
        oppNo = -2;
    }

    if (oppNo != -2) {
        const PlayerPtrCont::const_iterator end = wm.opponentsFromSelf().end();
        for (PlayerPtrCont::const_iterator o = wm.opponentsFromSelf().begin();
             o != end;
             ++o) {
            if ((*o)->unum() == oppNo)
                if ((*o)->pos().x < homePos.x + 5.0)
                    S_opp = (*o)->pos();
                else {
                    oppNo = -2;
                    isMarking = false;
                    break;
                }
        }
    }

    const PlayerPtrCont &opps = wm.opponentsFromSelf();
    const PlayerObject *nearestOpp = (opps.empty() ? static_cast< PlayerObject * >( 0 ) : opps.front());
    const Vector2D opp = (nearestOpp ? nearestOpp->pos() : Vector2D(-1000.0, 0.0));

    double oppDist = opp.dist(homePos);

    if ((((((num == 7 && ball.y > 9.0) || (num == 8 && ball.y < -9.0)) && fabs(opp.y - me.y) < 4.0) ||
          ((num == 8 && ball.y > 9.0) || (num == 7 && ball.y < -9.0))) &&
         ball.x < -37.0 && ball.absY() > 9.0 &&
         oppDist < 9.0 && opp.x > homePos.x - 2.0 && me.dist(homePos) < 8.0) || isMarking) {
//      if( oppNo == -2 )
        target = opp + Vector2D::polar2vector(MarkDist, (ball - opp).th());
//      else if( S_opp.x != 0 )
//          target = S_opp + Vector2D::polar2vector( MarkDist , (ball-opp).th() );
//      else
//          return false;

        double dashPower = ServerParam::i().maxDashPower();
        double distThr = 0.4;

        if (!Body_GoToPoint(target, distThr, dashPower,
                            -1.0,    // prefered dash speed. negative value means no preferred speed
                            3,    // preferred reach cycle
                            true,  // save_recovery
                            15.0  // turn angle threshold
        ).execute(agent)) {
            AngleDeg bodyAngle = agent->world().ball().angleFromSelf();
            if (bodyAngle.degree() < 0.0)
                bodyAngle -= 90.0;
            else
                bodyAngle += 90.0;

            Vector2D turnPoint = me;
            turnPoint += Vector2D::polar2vector(10.0, bodyAngle);

            Body_TurnToPoint(turnPoint).execute(agent);
        }

        isMarking = true;
//        amIMarking = true;

        if (oppNo == -2)
            oppNo == nearestOpp->unum();


        agent->setNeckAction(new Neck_TurnToBallOrScan());
        return true;
    }
    return false;
}


bool isSoftMarking = false;

bool Bhv_Mark::softMark(rcsc::PlayerAgent *agent) {


    const WorldModel &wm = agent->world();
    int num = wm.self().unum();

    Vector2D homePos = Strategy::i().getPosition(wm.self().unum());
    Vector2D ball = wm.ball().pos();
    Vector2D me = wm.self().pos();
    const int self_min = wm.interceptTable()->selfReachCycle();
    const int mate_min = wm.interceptTable()->teammateReachCycle();
    const int opp_min = wm.interceptTable()->opponentReachCycle();

    {
        if (wm.self().unum() == 6 && ball.x < 10.0 && ball.absY() > 12.0 && opp_min < mate_min && ball.x > -36.0) {
            Vector2D opp = wm.opponentsFromSelf().front()->pos();
            float oDist = wm.opponentsFromSelf().front()->pos().dist(homePos);

            if (oDist < 8.0) {
                homePos = opp + Vector2D::polar2vector(1.0, (ball - opp).th());
                homePos.x -= 1.0;
                isSoftMarking = true;
            }
        }
    }

    if (num == 2 || num == 3)
        softMarkCenterBack(agent, homePos);

    if (num == 4 || num == 5)
        softMarkSideBack2(agent, homePos);

    if (isSoftMarking) {
        AngleDeg bodyAngle = 0.0;
        double dist_thr = wm.ball().distFromSelf() * 0.1;
        if (dist_thr < 1.0) dist_thr = 1.0;
        const double dash_power = Strategy::get_normal_dash_power(wm);

        if (!Body_GoToPoint(homePos, dist_thr, dash_power
        ).execute(agent)) {
            if (wm.self().unum() < 6 && homePos.x < -30.0 && homePos.x > -38.0 && homePos.absY() < 20.0 &&
                opp_min < mate_min && opp_min < 10 && ball.dist(me) < 30.0) {
                AngleDeg bodyAngle = agent->world().ball().angleFromSelf();
                if (bodyAngle.degree() < 0.0)
                    bodyAngle -= 90.0;
                else
                    bodyAngle += 90.0;

                Body_TurnToAngle(bodyAngle).execute(agent);
// 2011            Body_TurnToPoint( Vector2D(-49.0, 0.0) ).execute( agent );
            } else if (mate_min < opp_min &&
                       homePos.x > wm.offsideLineX() - 15.0 &&
                       wm.ball().pos().x > wm.offsideLineX() - 25.0 &&
                       //            wm.offsideLineX() < 30.0 &&
                       (wm.self().unum() > 8 || (wm.self().unum() > 5 && wm.ball().pos().x > 30.0))) {
                Body_TurnToPoint(Vector2D(wm.self().pos().x + 20.0, wm.self().pos().y)).execute(agent);
            } else if (wm.self().unum() < 6 && ball.x < wm.ourDefenseLineX() + 25.0 && mate_min > opp_min &&
                       opp_min < 10) {
                AngleDeg bodyAngle = agent->world().ball().angleFromSelf();

                if (homePos.x < -34.0 && homePos.x > -37.0)
                    Body_TurnToPoint(rcsc::Vector2D(-37.0, 0.0)).execute(agent);
//          else if ( bodyAngle.degree() < 0.0 )
//                 bodyAngle -= 90.0;
//          else
//                 bodyAngle += 90.0;
//             Body_TurnToAngle( bodyAngle ).execute( agent );
                else if (num == 4 || num == 5)
                    Body_TurnToPoint(rcsc::Vector2D(-50.0, 0.0)).execute(agent);
                else
                    Body_TurnToPoint(rcsc::Vector2D(-50.0, 0.0)).execute(agent);

            } else if (espBodyTurns( agent, bodyAngle))

                Body_TurnToAngle(bodyAngle).execute(agent);
            else
                Body_TurnToBall().execute(agent);

        }

        dlog.addText(Logger::TEAM,
                     __FILE__": Bhv_Soft Mark");
        agent->setNeckAction(new Neck_ForMark());



        return true;
    }

    return false;
}


void Bhv_Mark::softMarkSideBack2(PlayerAgent *agent, Vector2D &homePos) {
    const WorldModel &wm = agent->world();

    Vector2D ball = wm.ball().pos();
    Vector2D me = wm.self().pos();

    int self_min = wm.interceptTable()->selfReachCycle();
    int mate_min = wm.interceptTable()->teammateReachCycle();
    int opp_min = wm.interceptTable()->opponentReachCycle();

    const PlayerPtrCont &opps = wm.opponentsFromSelf();
    const PlayerObject *nearest_opp = (opps.empty() ? static_cast< PlayerObject * >( 0 ) : opps.front());
    Vector2D opp = (nearest_opp ? nearest_opp->pos() : Vector2D(-1000.0, 0.0));

    Vector2D oppFhome = Vector2D(0, 0);

    int oppConf = wm.opponentsFromSelf().front()->posCount();
    int num = wm.self().unum();


    const PlayerPtrCont::const_iterator o2_end = wm.opponentsFromBall().end();
    for (PlayerPtrCont::const_iterator o2 = wm.opponentsFromBall().begin();
         o2 != o2_end;
         ++o2) {
        Vector2D oPos = (*o2)->pos() + (*o2)->vel();

        if (oPos.dist(opp) < 1.0)
            continue;

        if (oPos.x < wm.ourDefenseLineX() + 10.0 && ((num == 5 && oPos.y > opp.y + 0.1) ||
                                                     (num == 4 && oPos.y < opp.y - 0.1))) {
            opp = oPos;
        }

    }


    bool amInearest = true;

    double minDist = 100.0;

    const PlayerPtrCont::const_iterator o_end = wm.opponentsFromBall().end();
    for (PlayerPtrCont::const_iterator o = wm.opponentsFromBall().begin();
         o != o_end;
         ++o) {

        Vector2D opos = (*o)->pos() + (*o)->vel();

        if ((*o)->posCount() > 10)
            continue;

        if (opos.dist(homePos) < minDist) {
            oppFhome = opos;
            minDist = opos.dist(homePos);
        }

    }

    const PlayerPtrCont::const_iterator t_end = wm.teammatesFromSelf().end();
    for (PlayerPtrCont::const_iterator t = wm.teammatesFromSelf().begin();
         t != t_end;
         ++t) {
        Vector2D tpos = (*t)->pos() + (*t)->vel();

        if ((*t)->posCount() > 10) // round 2 IO2011: > 5
            continue;

        if (tpos.dist(opp) < me.dist(opp)) {
            amInearest = false;
            break;
        }
    }


    static bool marking = false;

    if (marking) {

        if ((wm.self().unum() == 4 && opp.y > homePos.y + 6.5) ||
            (wm.self().unum() == 5 && opp.y < homePos.y - 6.5)) {
            marking = false;
        } else if (agent->world().gameMode().type() != GameMode::PlayOn) {
            marking = false;
        } else if (nearest_opp->posCount() > 12) {
            marking = false;
        } else if (ball.x < -35.0) {
            marking = false;
        } else if (ball.x < -30.0 && std::fabs(ball.y - homePos.y) < 2.0) {
            marking = false;
        } else if (mate_min < opp_min + 1) // 2011: opp_min + 3
        {
            marking = false;
        } else if (opp_min > 30) {
            marking = false;
        } else if (ball.x > 10.0) {
            marking = false;
        } else {

            if ((wm.self().unum() == 4 && opp.y < 0.0) ||
                (wm.self().unum() == 5 && opp.y > 0.0))
                homePos.y = opp.y * 0.90; // 2011: * 0.975
            else
                homePos.y = opp.y * 1.15; // 2011: 1.1025

            marking = true;
            isSoftMarking = true;

            if (opp.x - 5.0 < homePos.x && std::fabs(ball.y - opp.y) > 10.0)
                homePos.x = opp.x - 6.0;
            else if (opp.x - 4.5 < homePos.x && std::fabs(ball.y - opp.y) > 7.0)
                homePos.x = opp.x - 5.0;
            else if (opp.x - 3.0 < homePos.x && std::fabs(ball.y - opp.y) > 5.0)
                homePos.x = opp.x - 4.0;
            else if (opp.x - 2.0 < homePos.x) // && std::fabs(ball.y-opp.y) > 2.5 )
                homePos.x = opp.x - 3.0;


            if (homePos.x < wm.ourDefenseLineX() - 4.0)
                homePos.x = wm.ourDefenseLineX() - 4.0;


            if (homePos.x < -36.0) // was -37.0 before 2011
                homePos.x = -36.0;
            return;
        }


    }


    if (homePos.x > -36.5 && me.x < homePos.x + 4.0 && oppConf < 9 && opp.x < homePos.x + 10.0 &&
        opp.x > homePos.x - 3.0 && opp.x > wm.ourDefenseLineX() - 1.5) {

        if ((wm.self().unum() == 4 && opp.y < homePos.y + 2.8) ||
            (wm.self().unum() == 5 && opp.y > homePos.y - 2.8)) {

//       std::cout<<"\nSoftMark for Side Executed! Cycle: "<<wm.time().cycle()<<" for "<<wm.self().unum()<<"\n";
            if ((wm.self().unum() == 4 && opp.y < 0.0) ||
                (wm.self().unum() == 5 && opp.y > 0.0))
                homePos.y = opp.y * 0.9; // 2011: * 0.975
            else
                homePos.y = opp.y * 1.15; // 2011: * 0.1025 !!


            marking = true;

            if (opp.x - 5.0 < homePos.x && std::fabs(ball.y - opp.y) > 10.0)
                homePos.x = opp.x - 6.0;
            else if (opp.x - 4.5 < homePos.x && std::fabs(ball.y - opp.y) > 7.0)
                homePos.x = opp.x - 5.0;
            else if (opp.x - 3.0 < homePos.x && std::fabs(ball.y - opp.y) > 5.0)
                homePos.x = opp.x - 4.0;
            else if (opp.x - 2.0 < homePos.x) // && std::fabs(ball.y-opp.y) > 2.5 )
                homePos.x = opp.x - 3.0;

            if (homePos.x < wm.ourDefenseLineX() - 4.0)
                homePos.x = wm.ourDefenseLineX() - 4.0;

            if (homePos.x < -36.0)  // was -37.0 before 2011
                homePos.x = -36.0;

            isSoftMarking = true;
            return;
        } else {
            marking = false;
            isSoftMarking = false;
            return;
        }

    } else {
        marking = false;
        isSoftMarking = false;
        return;
    }
    return;
}

void Bhv_Mark::softMarkCenterBack(PlayerAgent *agent, Vector2D &homePos) {
    const WorldModel &wm = agent->world();

    Vector2D ball = wm.ball().pos();
    Vector2D me = wm.self().pos();

    int self_min = wm.interceptTable()->selfReachCycle();
    int mate_min = wm.interceptTable()->teammateReachCycle();
    int opp_min = wm.interceptTable()->opponentReachCycle();

    const PlayerPtrCont &opps = wm.opponentsFromSelf();
    const PlayerObject *nearest_opp
            = (opps.empty()
               ? static_cast< PlayerObject * >( 0 )
               : opps.front());

    const Vector2D opp = (nearest_opp
                          ? nearest_opp->pos()
                          : Vector2D(-1000.0, 0.0));

    Vector2D oppFhome = Vector2D(0, 0);

    double minDist = 100.0;

    bool amInearest = true;

    const PlayerPtrCont::const_iterator o_end = wm.opponentsFromBall().end();
    for (PlayerPtrCont::const_iterator o = wm.opponentsFromBall().begin();
         o != o_end;
         ++o) {

        Vector2D opos = (*o)->pos() + (*o)->vel();

        if ((*o)->posCount() > 10)
            continue;

        if (opos.dist(homePos) < minDist) {
            oppFhome = opos;
            minDist = opos.dist(homePos);
        }

    }

    const PlayerPtrCont::const_iterator t_end = wm.teammatesFromSelf().end();
    for (PlayerPtrCont::const_iterator t = wm.teammatesFromSelf().begin();
         t != t_end;
         ++t) {

        Vector2D tpos = (*t)->pos() + (*t)->vel();

        if ((*t)->posCount() > 7)
            continue;

        if (tpos.dist(opp) < me.dist(opp) && tpos.x < opp.x) {
            amInearest = false;
            break;
        }

    }


    static bool marking = false;
    static int oNo = -1;


    if (marking && oNo != -1) {
        if (agent->world().gameMode().type() != GameMode::PlayOn) {
            marking = false;
            oNo = -1;
        } else if (nearest_opp->unum() != oNo) {
            marking = false;
            oNo = -1;
        } else if (nearest_opp->posCount() > 15) {
            marking = false;
            oNo = -1;
        } else if (ball.x < -35.0) {
            marking = false;
            oNo = -1;
        } else if (ball.x < -30.0 && std::fabs(ball.y - homePos.y) < 4.0 && std::fabs(me.y - homePos.y) > 3.0) {
            marking = false;
            oNo = -1;
        } else if (mate_min < opp_min + 1) {
            marking = false;
            oNo = -1;
        } else if (opp_min > 20) {
            marking = false;
            oNo = -1;
        } else if (ball.x > 10.0) {
            marking = false;
            oNo = -1;
        } else {
            homePos.y = opp.y;

            oNo = nearest_opp->unum();
            marking = true;
            isSoftMarking = true;

            if (opp.x - 5.0 < homePos.x && std::fabs(ball.y - opp.y) > 10.0)
                homePos.x = opp.x - 6.0;
            else if (opp.x - 4.5 < homePos.x && std::fabs(ball.y - opp.y) > 7.0)
                homePos.x = opp.x - 5.0;
            else if (opp.x - 3.0 < homePos.x && std::fabs(ball.y - opp.y) > 5.0)
                homePos.x = opp.x - 4.0;
            else if (opp.x - 2.0 < homePos.x) // && std::fabs(ball.y-opp.y) > 2.5 )
                homePos.x = opp.x - 3.0;

            if (homePos.x < wm.ourDefenseLineX() - 4.0)
                homePos.x = wm.ourDefenseLineX() - 4.0;

            if (homePos.x < -36.0)  // was -37.0 before 2011
                homePos.x = -36.0;

            return;
        }

    }

    float xDiff = 12.0;

    if (fabs(ball.x - homePos.x) < 15.0)
        xDiff = 10.0;
    if (fabs(ball.x - homePos.x) < 10.0)
        xDiff = 5.0;

    float yDiff = 7.0;

    if (homePos.x < -33.0)
        yDiff = 4.5;
    if (homePos.x < -30.0)
        yDiff = 5.0;
    if (homePos.x < -20.0)
        yDiff = 8.0;


    if (homePos.x > -36.5 && me.x > homePos.x - 4.0 && me.x < homePos.x + 2.0 &&
        opp.x < homePos.x + xDiff && opp.x > homePos.x - 2.0 && opp.x > wm.ourDefenseLineX() - 1.5)//&& amInearest )
    {
//       std::cout<<"\nSoftMark for Center Executed! Cycle: "<<wm.time().cycle()<<" for "<<wm.self().unum()<<"\n";
        if ((wm.self().unum() == 2 && opp.y < homePos.y + (yDiff - 2.0) && opp.y > homePos.y - yDiff) ||
            (wm.self().unum() == 3 && opp.y > homePos.y - (yDiff - 2.0) && opp.y < homePos.y + yDiff)) {
            homePos.y = opp.y;

            if (opp.x - 5.0 < homePos.x && std::fabs(ball.y - opp.y) > 10.0)
                homePos.x = opp.x - 6.0;
            else if (opp.x - 4.5 < homePos.x && std::fabs(ball.y - opp.y) > 7.0)
                homePos.x = opp.x - 5.0;
            else if (opp.x - 3.0 < homePos.x && std::fabs(ball.y - opp.y) > 5.0)
                homePos.x = opp.x - 4.0;
            else if (opp.x - 2.0 < homePos.x) // && std::fabs(ball.y-opp.y) > 2.5 )
                homePos.x = opp.x - 3.0;

            if (homePos.x < wm.ourDefenseLineX() - 4.0)
                homePos.x = wm.ourDefenseLineX() - 4.0;

            if (homePos.x < -36.0)  // was -37.0 before 2011
                homePos.x = -36.0;

            isSoftMarking = true;
        } else
            isSoftMarking = false;
    } else
        isSoftMarking = false;
}

bool Bhv_Mark::espBodyTurns(rcsc::PlayerAgent *agent, rcsc::AngleDeg & bodyAngle) {
    const WorldModel &wm = agent->world();

    Vector2D ball = wm.ball().pos();
    Vector2D me = wm.self().pos();

    int num = wm.self().unum();


    if ((num == 2 || num == 3) && ball.absY() > 5.0 && me.x < -48.0 && sign(me.y) != sign(ball.y)) {
        bodyAngle = (sign(ball.y) > 0.0 ? 90.0 : -90.0);
        return true;
    }


    return false;
}