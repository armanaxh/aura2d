//
// Created by armanaxh on ۲۰۱۹/۱۰/۳۰.
//

#ifndef AURA2D_BHV_OFFSIDE_TRAP_H
#define AURA2D_BHV_OFFSIDE_TRAP_H


#include <rcsc/player/soccer_action.h>

class Bhv_OffsideTrap
        : public rcsc::SoccerBehavior {
public:
    Bhv_OffsideTrap()
    { }

    bool execute( rcsc::PlayerAgent * agent );

private:
};


#endif //AURA2D_BHV_OFFSIDE_TRAP_H
