//
// Created by armanaxh on ۲۰۱۹/۱۰/۳۰.
//

#ifndef AURA2D_BHV_DEFENCE_H
#define AURA2D_BHV_DEFENCE_H

#include <rcsc/player/soccer_action.h>

class Bhv_Defense
        : public rcsc::SoccerBehavior {
public:
    Bhv_Defense()
    { }

    bool execute( rcsc::PlayerAgent * agent );

private:

};


#endif //AURA2D_BHV_DEFENCE_H
