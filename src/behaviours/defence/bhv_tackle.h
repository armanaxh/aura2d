//
// Created by armanaxh on ۲۰۱۹/۱۰/۳۰.
//

#ifndef AURA2D_BHV_BASIC_TACKLE_H
#define AURA2D_BHV_BASIC_TACKLE_H

#include <rcsc/player/soccer_action.h>

class Bhv_Tackle
        : public rcsc::SoccerBehavior {
public:
    Bhv_Tackle()
    { }

    bool execute( rcsc::PlayerAgent * agent );

private:
};

#endif //AURA2D_BHV_BASIC_TACKLE_H
