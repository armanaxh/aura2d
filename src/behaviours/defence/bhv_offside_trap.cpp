//
// Created by armanaxh on ۲۰۱۹/۱۰/۳۰.
//

#include "bhv_offside_trap.h"

#include "../../strategy.h"
#include <rcsc/action/neck_turn_to_ball_or_scan.h>
#include <rcsc/action/basic_actions.h>
#include <rcsc/action/body_go_to_point.h>
#include <rcsc/player/player_agent.h>
#include <rcsc/player/debug_client.h>
#include <rcsc/player/intercept_table.h>
#include <rcsc/common/logger.h>
#include <rcsc/common/server_param.h>

using namespace rcsc;

bool Bhv_OffsideTrap::execute(rcsc::PlayerAgent *agent) {
    dlog.addText(Logger::TEAM,
                 __FILE__": Bhv_OffsideTrap");

    const WorldModel &wm = agent->world();

    /*--------------------------------------------------------*/
    // chase ball
    const int self_min = wm.interceptTable()->selfReachCycle();
    const int mate_min = wm.interceptTable()->teammateReachCycle();
    const int opp_min = wm.interceptTable()->opponentReachCycle();


    // G2d: role
    int role = Strategy::i().roleNumber(wm.self().unum());


    const Vector2D target_point = Strategy::i().getPosition(wm.self().unum());
    Vector2D me = wm.self().pos();
    Vector2D homePos = target_point;
    int num = role;

// G2D : offside trap
    double first = 0.0, second = 0.0;

    const PlayerPtrCont::const_iterator t3_end = wm.teammatesFromSelf().end();
    for (PlayerPtrCont::const_iterator it = wm.teammatesFromSelf().begin();
         it != t3_end;
         ++it) {
        double x = (*it)->pos().x;
        if (x < second) {
            second = x;
            if (second < first) {
                std::swap(first, second);
            }
        }
    }

    double buf1 = 3.5;
    double buf2 = 4.5;

    if (me.x < -37.0 && opp_min < mate_min &&
        (homePos.x > -37.5 || wm.ball().inertiaPoint(opp_min).x > -36.0) &&
        second + buf1 > me.x && wm.ball().pos().x > me.x + buf2) {
        Body_GoToPoint(rcsc::Vector2D(me.x + 15.0, me.y),
                       0.5, ServerParam::i().maxDashPower(), // maximum dash power
                       ServerParam::i().maxDashPower(),     // preferred dash speed
                       2,                                  // preferred reach cycle
                       true,                              // save recovery
                       5.0).execute(agent);

        if (wm.existKickableOpponent()
            && wm.ball().distFromSelf() < 12.0)
            agent->setNeckAction(new Neck_TurnToBall());
        else
            agent->setNeckAction(new Neck_TurnToBallOrScan());
        return true;
    }

    return false;
}