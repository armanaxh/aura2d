//
// Created by armanaxh on ۲۰۱۹/۹/۸.
//

#include "bhv_intercept.h"

#include <rcsc/common/server_param.h>
#include <rcsc/common/logger.h>
#include <rcsc/player/world_model.h>
#include <rcsc/player/player_agent.h>
#include <rcsc/common/logger.h>
#include <rcsc/action/body_intercept.h>
#include <rcsc/action/neck_turn_to_ball_or_scan.h>
#include <rcsc/action/neck_turn_to_low_conf_teammate.h>
#include "../actions/neck_offensive_intercept_neck.h"
#include "./defence/bhv_tackle.h"
#include <rcsc/action/basic_actions.h>
#include <rcsc/action/body_go_to_point.h>

#include "../strategy.h"

using namespace rcsc;

bool Bhv_Intercept::execute(rcsc::PlayerAgent *agent) {


    dlog.addText(Logger::TEAM,
                 "execute Bhv_intercept");

    const ServerParam &SP = ServerParam::i();
    const WorldModel &wm = agent->world();
    const Strategy &stra = Strategy::i();


    const Vector2D self_pos = wm.self().pos();
    const Vector2D ball_pos = wm.ball().pos();
    const int self_unum = wm.self().unum();


    const int self_min = wm.interceptTable()->selfReachCycle();
    const int mate_min = wm.interceptTable()->teammateReachCycle();
    const int opp_min = wm.interceptTable()->opponentReachCycle();

    dlog.addText(Logger::TEAM,
                 __FILE__": ================= self_min : %d , mate_min: %d ,  opp_min: %d", self_min, mate_min,
                 opp_min);


    Vector2D ball = wm.ball().pos();
    Vector2D me = wm.self().pos();
    const Vector2D target_point = Strategy::i().getPosition(wm.self().unum());

    Vector2D homePos = target_point;

    int num = wm.self().unum();
    Vector2D myInterceptPos = wm.ball().inertiaPoint(self_min);

    bool intercept_flag = false;

    {
        if (num > 5 && !wm.existKickableTeammate()
            && (self_min <= 3
                || (self_min < mate_min + 3
                    && self_min < opp_min + 4)
            )
                ) {
            intercept_flag = true;
        }

        if (num > 6 && (wm.existKickableOpponent() || (opp_min < 5 && mate_min > opp_min)) &&
            (ball.dist(homePos) < 10.0 || (self_min < 6 && ball.dist(homePos) < 15.0) ||
             (self_min < 6 && ball.dist(homePos) < 25.0 && num > 8))
            && ball.x > -36.0 && wm.self().stamina() > 4000.0 && !wm.existKickableTeammate()) {
            intercept_flag = true;
        }

    }

    {
        if (num < 6 && !wm.existKickableTeammate()
            && (self_min <= 2
                || (self_min <= mate_min // self_min < mate_min + 3 bud!
                    && self_min < opp_min + 2) // base: self_min < opp_min + 4
            )
                ) {
            intercept_flag = true;
        }

        if ((num == 4 || num == 5) && self_min <= 4 && ball.absY() > 15.0 && ball.x < -36.0 &&
            me.dist(Vector2D(-50.0, 0.0)) < ball.dist(Vector2D(-50.0, 0.0)) &&
            !wm.existKickableTeammate()) {
            intercept_flag = true;
        }
    }

    {
        if ((num > 6 && mate_min > self_min + 1 && self_min < opp_min + 3 &&
             myInterceptPos.x > 15.0) ||
            (num == 11 && mate_min > self_min && self_min < opp_min + 6 &&
             myInterceptPos.x > 5.0 && myInterceptPos.x > wm.offsideLineX() - 3) ||
            (num > 6 && myInterceptPos.x > 20.0 && self_min < mate_min + 3 && opp_min < mate_min &&
             opp_min < self_min && !wm.existKickableTeammate())
                ) {

            intercept_flag = true;
        }
    }


    if(self_min == mate_min){
        const PlayerObject * teammate = wm.interceptTable()->fastestTeammate();
        if(teammate != NULL){
            const int mate_unum = teammate->unum();
            if(mate_unum > 0 && mate_unum <= 11){
                const int self_unum = wm.self().unum();
                if(self_unum > mate_unum){
                    intercept_flag = false;
                }else{
                    intercept_flag = true;
                }
            }
        }
    }

    if(self_min > mate_min){
        intercept_flag = false;
    }


    if (intercept_flag) {
        Body_Intercept().execute(agent);
        agent->setNeckAction(new Neck_OffensiveInterceptNeck());
        return true;
    }

    {
        int goalCycles = 100;

        for (int z = 1; z < 15; z++)
            if (wm.ball().inertiaPoint(z).x < -52.5 && wm.ball().inertiaPoint(z).absY() < 8.0) {
                goalCycles = z;
                break;
            }

        if ((wm.self().unum() == 2 || wm.self().unum() == 3) && goalCycles != 100 &&
            mate_min >= goalCycles && me.x < -45.0 && !wm.existKickableTeammate()) {

            if (wm.ball().inertiaPoint(self_min).x > -52.0)
                Body_Intercept().execute(agent);
            else
                Body_GoToPoint(wm.ball().inertiaPoint(goalCycles - 1),
                               0.5, ServerParam::i().maxDashPower()).execute(agent);

            agent->setNeckAction(new Neck_OffensiveInterceptNeck());
            return true;
        }
    }

    return false;
}